﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigitalSummitInvitation
{
    public enum Side
    {
        Top,
        Bottom,
        Left,
        Right
    }
}

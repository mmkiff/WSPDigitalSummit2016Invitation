﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigitalSummitInvitation
{
    public class Line
    {
        public Vector2 P1;
        public Vector2 P2;

        public Line(Vector2 p1, Vector2 p2)
        {
            P1 = p1;
            P2 = p2;
        }

        public static Vector2? GetIntersection(Line AB, Line CD)
        {
            double deltaACy = AB.P1.Y - CD.P1.Y;
            double deltaDCx = CD.P2.X - CD.P1.X;
            double deltaACx = AB.P1.X - CD.P1.X;
            double deltaDCy = CD.P2.Y - CD.P1.Y;
            double deltaBAx = AB.P2.X - AB.P1.X;
            double deltaBAy = AB.P2.Y - AB.P1.Y;

            double denominator = deltaBAx * deltaDCy - deltaBAy * deltaDCx;
            double numerator = deltaACy * deltaDCx - deltaACx * deltaDCy;

            if (denominator == 0)
            {
                if (numerator == 0)
                {
                    // collinear. Potentially infinite intersection points.
                    // Check and return one of them.
                    if (AB.P1.X >= CD.P1.X && AB.P1.X <= CD.P2.X)
                    {
                        return AB.P1;
                    }
                    else if (CD.P1.X >= AB.P1.X && CD.P1.X <= AB.P2.X)
                    {
                        return CD.P1;
                    }
                    else
                    {
                        return null;
                    }
                }
                else
                { // parallel
                    return null;
                }
            }

            double r = numerator / denominator;
            if (r < 0 || r > 1)
            {
                return null;
            }

            double s = (deltaACy * deltaBAx - deltaACx * deltaBAy) / denominator;
            if (s < 0 || s > 1)
            {
                return null;
            }

            return new Vector2((float)(AB.P1.X + r * deltaBAx), (float)(AB.P1.Y + r * deltaBAy));
        }

        public double Length()
        {
            return Math.Sqrt(Math.Pow((P2.Y - P1.Y), 2) + Math.Pow((P2.X - P1.X), 2));
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace DigitalSummitInvitation.GameObjects
{
    public class PressAnyKeyPrompt : IGameObject
    {
        private Vector2 _position;
        private Texture2D _image;
        private float _alpha;
        private float _speed = 0f; // TO DO - try and time with the music
        private float _maxAlpha = 1.1f;
        private bool _disabled = false;

        public PressAnyKeyPrompt(Vector2 position)
        {
            _position = position;
            _image = Game1.ContentManager.Load<Texture2D>("pressAnyKey");
            _alpha = 0f;
        }

        public void Draw()
        {
            Game1.SpriteBatch.Draw(_image, _position, new Color(Color.White, _alpha));
        }

        public void Update(GameTime gameTime)
        {
            float amountToChange = _speed * (float)gameTime.ElapsedGameTime.TotalSeconds;

            _alpha += amountToChange;
            if (_alpha > _maxAlpha)
            {
                _alpha = _maxAlpha;
                _speed = _speed * -1;
            }
            else if (_alpha < 0f)
            {
                _alpha = 0;
                _speed = _speed * -1;
                if (_disabled)
                    _maxAlpha = 0f;
            }
        }

        public void Hide()
        {
            if (_speed > 0)
                _speed = _speed * -1;
            _disabled = true;
        }

        public void Start()
        {
            _speed = 1.5f;
        }
    }

    
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;

namespace DigitalSummitInvitation.GameObjects
{
    public class Brick : IGameObject
    {
        
        public Vector2 Position;        
        public bool Alive;
        public static int WIDTH;
        private Color _color;
        private SoundEffect _destroySound;
        private Texture2D _image;

        public Brick(Vector2 position, Color color)
        {
            _image = Game1.ContentManager.Load<Texture2D>("whiteBrick");
            Brick.WIDTH = _image.Width; // kind of silly, but we want this set to the actual image width rather than just hardcoding it.
            _destroySound = Game1.ContentManager.Load<SoundEffect>("brickDestroy");
            Position = position;
            _color = color;
            Alive = true;
        }

        public void Draw()
        {
            if(Alive)
                Game1.SpriteBatch.Draw(_image, Position, _color); 
        }

        public void Update(GameTime gameTime)
        {
        }

        public Rectangle GetCollisionRectangle()
        {
            return new Rectangle((int)Position.X, (int)Position.Y, _image.Width, _image.Height);
        }

        public Line GetLeftBoundaryLine()
        {
            return new Line(new Vector2(Position.X, Position.Y), new Vector2(Position.X, Position.Y + _image.Height));
        }

        public Line GetRightBoundaryLine()
        {
            return new Line(new Vector2(Position.X + _image.Width, Position.Y), new Vector2(Position.X + _image.Width, Position.Y + _image.Height));
        }

        public Line GetTopBoundaryLine()
        {
            return new Line(new Vector2(Position.X, Position.Y), new Vector2(Position.X + _image.Width, Position.Y));
        }

        public Line GetBottomBoundaryLine()
        {
            return new Line(new Vector2(Position.X, Position.Y + _image.Height), new Vector2(Position.X + _image.Width, Position.Y + _image.Height));
        }

        public void Destroy()
        {
            if (Alive)
                _destroySound.Play();
            Alive = false;
        }
    }
}

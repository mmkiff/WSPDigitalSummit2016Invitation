﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace DigitalSummitInvitation.GameObjects
{
    public class Text : IGameObject
    {
        private string _text;
        private Vector2 _position;

        public Text(string text, Vector2 centerPosition)
        {
            _text = text;
            Vector2 size = Fonts.BrickFront.MeasureString(text);
            _position = new Vector2(centerPosition.X - (size.X / 2), centerPosition.Y - (size.Y / 2));
        }

        public void Draw()
        {
            Game1.SpriteBatch.DrawString(Fonts.BrickFront, _text, _position, Colors.InviteTextColor);
        }

        public void Update(GameTime gameTime)
        {
        }
    }
}

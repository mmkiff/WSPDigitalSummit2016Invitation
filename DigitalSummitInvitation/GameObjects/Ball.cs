﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;

namespace DigitalSummitInvitation.GameObjects
{
    public class Ball : IGameObject
    {
        private Texture2D _image;
        public Vector2 Position;
        public Vector2 PreviousPosition;

        private readonly float MIN_SPEED = 4f;

        private SoundEffect _hitSound;
        private SoundEffect _hitWallSound;
        private float _rotation;
        private float _speed = 5f;        
        private TimeSpan _timeOfLastPaddleHit = TimeSpan.MinValue;        

        public Ball(Vector2 position, float rotation)
        {
            _image = Game1.ContentManager.Load<Texture2D>("ball");
            Position = position;
            PreviousPosition = new Vector2(position.X, position.Y);
            _rotation = rotation;
            
            _hitSound = Game1.ContentManager.Load<SoundEffect>("ballHit");
            _hitWallSound = Game1.ContentManager.Load<SoundEffect>("wallHit");
        }

        public void Draw()
        {
            Game1.SpriteBatch.Draw(_image, Position, Color.White);
        }

        public void Update(GameTime gameTime)
        {
            float amountToMove = _speed * (float)gameTime.ElapsedGameTime.TotalSeconds;

            PreviousPosition = new Vector2(Position.X, Position.Y);

            Position.X = (float)(Position.X + Math.Cos(_rotation) * _speed);
            Position.Y = (float)(Position.Y + Math.Sin(_rotation) * _speed);

            bool hitWall = false;
            if (Position.X > Game1.WIDTH - 5 - _image.Width)
            {
                hitWall = true;
                _rotation = (float)(Math.PI - _rotation);
            }
            if (Position.X < 5)
            {
                hitWall = true;
                _rotation = (float)(Math.PI - _rotation);
                Position.X = 7;
            }                
            if(Position.Y < 5)
            {
                hitWall = true;
                _rotation = (float)(Math.PI * 2 - _rotation);
            }
            if (Position.Y > Game1.HEIGHT)
            {
                Position = new Vector2(100, 10);
                _speed -= 2f;
                if (_speed < MIN_SPEED)
                    _speed = MIN_SPEED;
            }

            if (hitWall)
                _hitWallSound.Play();
        }

        public Rectangle GetCollisionRectangle()
        {
            return new Rectangle((int)Position.X, (int)Position.Y, _image.Width, _image.Height);
        }

        public Line GetTrajectoryLine()
        {
            return new Line(new Vector2(PreviousPosition.X +(_image.Width / 2), PreviousPosition.Y + (_image.Height / 2)), GetCenterPosition());
        }

        public Vector2 GetCenterPosition()
        {
            return new Vector2(Position.X + (_image.Width / 2), Position.Y + (_image.Height / 2));
        }

        public void HitBrick(Side side)
        {
            if(side == Side.Top || side == Side.Bottom)
                _rotation = (float)(Math.PI * 2 - _rotation);
            else
                _rotation = (float)(Math.PI - _rotation);
        }

        public void HitPaddle(GameTime gameTime, float newRotation)
        {
            if(_timeOfLastPaddleHit == TimeSpan.MinValue)
                _timeOfLastPaddleHit = gameTime.ElapsedGameTime.Subtract(new TimeSpan(0, 1, 0));

            TimeSpan timeSinceLast = gameTime.TotalGameTime - _timeOfLastPaddleHit;

            if (timeSinceLast.TotalMilliseconds > 500f)
            {
                _rotation = newRotation;
                _rotation = (float)(Math.PI * 2 - _rotation);
                _timeOfLastPaddleHit = gameTime.TotalGameTime;
                _hitSound.Play();

                float bigSpeedChange = 3;
                float smallSpeedChange = 1;
                if (newRotation < 0.523599f) // 30
                    _speed += bigSpeedChange;
                else if (newRotation < 0.872665f) // 50
                    _speed += smallSpeedChange;
                else if (newRotation > 2.61799f) // 150
                    _speed += bigSpeedChange;
                else if (newRotation > 2.26893f) // 130
                    _speed += smallSpeedChange;
                else if (newRotation > 1.39626f && newRotation < 1.74533f) // 80 and 100
                    _speed -= bigSpeedChange;

                if (_speed < MIN_SPEED)
                    _speed = MIN_SPEED;
            }
        }
    }
}

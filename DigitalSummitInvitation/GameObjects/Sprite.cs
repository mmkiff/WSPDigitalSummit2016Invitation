﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace DigitalSummitInvitation.GameObjects
{
    public class Sprite : IGameObject
    {
        public event TransitionEvent FadeInComplete;
        public event TransitionEvent FadeOutComplete;
        private Texture2D _image;
        private Vector2 _position;
        private float _alpha;
        private Status _status;
        private float _speed;

        public delegate void TransitionEvent(IGameObject sender);

        public Sprite(string imageName, Vector2 position, float alpha)
        {
            _image = Game1.ContentManager.Load<Texture2D>(imageName);
            _position = position;
            _alpha = alpha;
            _status = Status.Waiting;
        }

        public void Draw()
        {
            Game1.SpriteBatch.Draw(_image, _position, new Color(Color.White, _alpha));
        }

        public void Update(GameTime gameTime)
        {
            if(_status == Status.FadingIn)
            {
                float amountToChange = _speed * (float)gameTime.ElapsedGameTime.TotalSeconds;
                _alpha += amountToChange;
                if(_alpha > 1)
                {
                    _status = Status.Waiting;
                    if (FadeInComplete != null)
                        FadeInComplete(this);
                }
            }
            else if(_status == Status.FadingOut)
            {
                float amountToChange = _speed * (float)gameTime.ElapsedGameTime.TotalSeconds;
                _alpha -= amountToChange;
                if (_alpha < 0)
                {
                    _status = Status.Waiting;
                    if (FadeOutComplete != null)
                        FadeOutComplete(this);
                }
            }
        }

        public void FadeOut(float speed)
        {
            _status = Status.FadingOut;
            _speed = speed;
        }

        public void FadeIn(float speed)
        {
            _status = Status.FadingIn;
            _speed = speed;
        }

        private enum Status
        {
            FadingIn,
            FadingOut,
            Waiting
        }
    }
}

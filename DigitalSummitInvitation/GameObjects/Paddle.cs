﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigitalSummitInvitation.GameObjects
{
    public class Paddle : IGameObject
    {
        private Texture2D _image;
        public Vector2 Position;
        public readonly int WIDTH;
        private bool _inputDisabled;

        public Paddle(bool disableInput = false)
        {
            _image = Game1.ContentManager.Load<Texture2D>("paddle");
            WIDTH = _image.Width;
            Position = new Vector2(30, 550);
            _inputDisabled = disableInput;
        }

        public void Draw()
        {
            Game1.SpriteBatch.Draw(_image, Position, Color.White);
        }

        public void Update(GameTime gameTime)
        {
            if (!_inputDisabled)
            {
                MouseState state = Mouse.GetState();
                Position.X = ConvertMouseXToGameX(state.X); // - 38; // put it in the halfway point of the paddle
            }
        }

        private int ConvertMouseXToGameX(float x)
        {
            float xPercent = x / Game1.ACTUAL_WIDTH;

            int result = (int)(Game1.WIDTH * xPercent);

            Console.WriteLine(string.Format("{0} -> {1}% = {2} - actual width {3}", x, xPercent, result, Game1.ACTUAL_WIDTH));

            return result;
        }

        public Rectangle GetCollisionRectangle()
        {
            return new Rectangle((int)Position.X + 4, (int)Position.Y, _image.Width - 4, _image.Height);
        }

        public float GetBallModifier(float impactX)
        {
            float centerOfImpact = impactX + 6; // 6 is half the ball width
            float result;
            float startRadians = 0.174533f; // 10 degrees
            float maxRadians = 2.96706f; // 170 degrees
            float adjustedMaxRadians = maxRadians - startRadians;
            float impactModifier = impactX - Position.X;

            if (impactModifier < 0)
                impactModifier = 0;
            if (impactModifier > _image.Width)
                impactModifier = _image.Width;

            impactModifier = 1 - (impactModifier / _image.Width);


            result = (impactModifier * adjustedMaxRadians) + startRadians;

            return result;
        }
    }
}

﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigitalSummitInvitation.Screens
{
    public interface IScreen
    {
        void Update(GameTime gameTime);
        void Draw();
    }
}

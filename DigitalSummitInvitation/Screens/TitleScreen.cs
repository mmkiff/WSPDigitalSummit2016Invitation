﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using DigitalSummitInvitation.GameObjects;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Audio;
using System.Timers;

namespace DigitalSummitInvitation.Screens
{
    public class TitleScreen : IScreen
    {
        public event StartGameEvent StartGame;
        public event StartGameEvent StartDemo;
        private Texture2D _backgroundImage;
        private PressAnyKeyPrompt _pressAnyKeyPrompt;
        private Sprite _digitalSummit2016Text;
        private Sprite _versionText;
        private Sprite _logo;
        private Song _music;
        private SoundEffect _keyPressedSound;
        private float _fadeOutSpeed;
        private Status _status;
        private float _volume;
        private Timer _idleTimer;

        public delegate void StartGameEvent(IScreen sender);
        
        public TitleScreen()
        {
            _fadeOutSpeed = 1.2f;
            _volume = 1f;
            _status = Status.FadingIn;
            _backgroundImage = Game1.ContentManager.Load<Texture2D>("blackBackground");
            _digitalSummit2016Text = new Sprite("digitalSummit2016", new Vector2(141, 169), 0);
            _digitalSummit2016Text.FadeInComplete += _digitalSummit2016Text_FadeInComplete;

            _logo = new Sprite("logo", new Vector2(133, 198), 0);
            _logo.FadeInComplete += _logo_FadeInComplete;

            _versionText = new Sprite("versionText", new Vector2(197, 331), 0);
            _versionText.FadeInComplete += _versionText_FadeInComplete;

            _music = Game1.ContentManager.Load<Song>("633802_Star-Command");
            _keyPressedSound = Game1.ContentManager.Load<SoundEffect>("startGame");
            _pressAnyKeyPrompt = new PressAnyKeyPrompt(new Vector2(239, 505));
            MediaPlayer.IsRepeating = false;
            MediaPlayer.Volume = _volume;
            MediaPlayer.Play(_music);
            

            _digitalSummit2016Text.FadeIn(0.3f);
        }

        private void _logo_FadeInComplete(IGameObject sender)
        {
            _versionText.FadeIn(0.3f);
            _pressAnyKeyPrompt.Start();
        }

        private void _versionText_FadeInComplete(IGameObject sender)
        {
            _idleTimer = new Timer(5000);
            _idleTimer.Elapsed += IdleTimer_Elapsed;
            _idleTimer.AutoReset = false;
            _idleTimer.Enabled = true;
        }

        private void IdleTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (_status != Status.FadingOut && _status != Status.Inactive)
            {
                Console.WriteLine("Starting demo.");
                if (StartDemo != null)
                    StartDemo(this);
            }
        }

        private void _digitalSummit2016Text_FadeInComplete(IGameObject sender)
        {
            _logo.FadeIn(0.3f);
            _status = Status.Waiting;
        }

        public void Draw()
        {
            Game1.SpriteBatch.Draw(_backgroundImage, new Vector2(0, 0), Color.White);
            _digitalSummit2016Text.Draw();
            _versionText.Draw();
            _logo.Draw();
            _pressAnyKeyPrompt.Draw();
        }

        public void Update(GameTime gameTime)
        {
            _digitalSummit2016Text.Update(gameTime);
            _versionText.Update(gameTime);
            _logo.Update(gameTime);
            if (_status == Status.Waiting)
            {
                if (Keyboard.GetState().GetPressedKeys().Count() > 0 || Mouse.GetState().LeftButton == ButtonState.Pressed )
                {
                    _keyPressedSound.Play();
                    FadeAllOut();
                }
                _pressAnyKeyPrompt.Update(gameTime);
            }
            else if (_status == Status.FadingOut)
            {
                float amountToChange = _fadeOutSpeed * (float)gameTime.ElapsedGameTime.TotalSeconds;

                _volume -= amountToChange;
                MediaPlayer.Volume = _volume;
                _pressAnyKeyPrompt.Update(gameTime);
                if (_volume <= 0)
                {
                    _status = Status.Inactive;
                    if (StartGame != null)
                        StartGame(this);
                }
            }
            else if (_status == Status.Inactive)
                _pressAnyKeyPrompt.Update(gameTime);
        }

        private void FadeAllOut()
        {
            _status = Status.FadingOut;
            _pressAnyKeyPrompt.Hide();
            _logo.FadeOut(_fadeOutSpeed);
            _digitalSummit2016Text.FadeOut(_fadeOutSpeed);
            _versionText.FadeOut(_fadeOutSpeed);
        }

        private enum Status
        {
            Waiting,
            FadingOut,
            FadingIn,
            Inactive
        }
    }
}

﻿using DigitalSummitInvitation.GameObjects;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;

namespace DigitalSummitInvitation.Screens
{
    public class GameplayScreen : IScreen
    {
        public event ScreenEvent DemoOver;
        public event ScreenEvent GameOver;

        private Paddle _paddle;
        private Ball _ball;
        private Sprite _background;
        private Sprite _fadeOutScreen;
        private Song _backgroundMusic;
        private List<Brick> _bricks;
        private bool _demoMode;
        private float _demoModePaddleOffset;
        private Sprite _gameOverScreen;
        private bool _gameOver;

        public delegate void ScreenEvent(IScreen sender);

        public GameplayScreen(bool demoMode)
        {
            _gameOver = false;
            _demoMode = demoMode;
            _demoModePaddleOffset = 10f;
            _background = new Sprite("background", new Vector2(0, 0), 1f);
            _fadeOutScreen = new Sprite("blackBackground", new Vector2(0, 0), 0f);
            _gameOverScreen = new Sprite("gameOverBackground", new Vector2(0, 0), 0f);
            _paddle = new Paddle(demoMode);
            _ball = new Ball(new Vector2(100, 500), 4f);

            InitializeBricks();
            InitializeMusic();

            if (demoMode)
            {
                var timer = new Timer(30000);
                timer.Elapsed += DemoTimerCompleted;
                timer.AutoReset = false;
                timer.Enabled = true;
            }
        }

        private void EndDemo()
        {
            _fadeOutScreen.FadeIn(0.2f);
            _fadeOutScreen.FadeInComplete += _fadeOutScreen_FadeInComplete;
        }

        private void _fadeOutScreen_FadeInComplete(IGameObject sender)
        {
            if (DemoOver != null)
                DemoOver(this);
        }

        private void DemoTimerCompleted(object sender, ElapsedEventArgs e)
        {
            EndDemo();
        }

        private void InitializeMusic()
        {
            _backgroundMusic = Game1.ContentManager.Load<Song>("669134_Bona---Player-2");

            MediaPlayer.Volume = 0.5f;
            MediaPlayer.IsRepeating = true;
            MediaPlayer.Play(_backgroundMusic);
        }

        private void InitializeBricks()
        {
            _bricks = new List<Brick>();
            int spacing = 0;

            Brick b = new Brick(new Vector2(0, 0), Color.White); // stupid workaround to get it setting the correct width

            _bricks.AddRange(GetBricks(new Vector2(400 - (10 * (Brick.WIDTH + spacing) / 2), 50), 10, spacing, Colors.WSPBlue));

            _bricks.AddRange(GetBricks(new Vector2(400 - (12 * (Brick.WIDTH + spacing) / 2), 110), 12, spacing, Colors.WSPRed));

            _bricks.AddRange(GetBricks(new Vector2(400 - (5 * (Brick.WIDTH + spacing) / 2), 168), 5, spacing, Colors.WSPRed));
            _bricks.AddRange(GetBricks(new Vector2(400 - (9 * (Brick.WIDTH + spacing) / 2), 215), 9, spacing, Colors.WSPBlue));

            _bricks.AddRange(GetBricks(new Vector2(40, 300), 2, spacing, Colors.WSPRed));
            _bricks.AddRange(GetBricks(new Vector2(640, 300), 2, spacing, Colors.WSPRed));
        }

        public void Update(GameTime gameTime)
        {
            if (_demoMode)
            {
                _paddle.Position = new Vector2(_ball.Position.X - _demoModePaddleOffset, _paddle.Position.Y);

                if (Keyboard.GetState().GetPressedKeys().Count() > 0)
                    if (DemoOver != null)
                        DemoOver(this);
            }

            _background.Update(gameTime);

            if (_gameOver == false)
                _ball.Update(gameTime);

            _paddle.Update(gameTime);

            foreach (Brick brick in _bricks)
                brick.Update(gameTime);

            if(_gameOver == false)
                CheckForCollisions(gameTime);
            _fadeOutScreen.Update(gameTime);
            _gameOverScreen.Update(gameTime);
        }

        public void Draw()
        {
            _background.Draw();
            _ball.Draw();
            _paddle.Draw();

            foreach (Brick brick in _bricks)
                brick.Draw();

            _gameOverScreen.Draw();
            _fadeOutScreen.Draw();            
        }

        private void CheckForCollisions(GameTime gameTime)
        {
            Rectangle ball = _ball.GetCollisionRectangle();

            CheckForPaddleCollisions(gameTime, ball);
            CheckForBrickCollisionsLinePoints(gameTime, _ball.GetTrajectoryLine(), ball);
        }        

        private void CheckForBrickCollisionsLinePoints(GameTime gameTime, Line ballPath, Rectangle ball)
        {
            // this is almost definitely way more complicated than it needs to be.
            // please don't judge me.
            Vector2 ballMid = _ball.GetCenterPosition();

            foreach (Brick brick in _bricks)
            {
                if (brick.Alive)
                {
                    Rectangle brickRectangle = brick.GetCollisionRectangle();
                    Rectangle intersection = Rectangle.Intersect(ball, brickRectangle);

                    if (intersection.Width != 0 || intersection.Height != 0)
                    {
                        Line left = brick.GetLeftBoundaryLine();
                        Line right = brick.GetRightBoundaryLine();
                        Line top = brick.GetTopBoundaryLine();
                        Line bottom = brick.GetBottomBoundaryLine();

                        double leftDistance = 99999;
                        double rightDistance = 99999;
                        double topDistance = 99999;
                        double bottomDistance = 99999;

                        Vector2? leftIntersection = Line.GetIntersection(left, ballPath);
                        Vector2? rightIntersection = Line.GetIntersection(right, ballPath);
                        Vector2? topIntersection = Line.GetIntersection(top, ballPath);
                        Vector2? bottomIntersection = Line.GetIntersection(bottom, ballPath);

                        if (leftIntersection.HasValue)
                            leftDistance = new Line(ballMid, leftIntersection.Value).Length();
                        if (rightIntersection.HasValue)
                            rightDistance = new Line(ballMid, rightIntersection.Value).Length();
                        if (topIntersection.HasValue)
                            topDistance = new Line(ballMid, topIntersection.Value).Length();
                        if (bottomIntersection.HasValue)
                            bottomDistance = new Line(ballMid, bottomIntersection.Value).Length();

                        if (leftIntersection == null && rightIntersection == null && bottomIntersection == null && topIntersection == null)
                            return;

                        // Console.WriteLine(string.Format("Top distance: {0}, Bottom distance: {1}, Left Distance: {2}, Right Distance: {3}", topDistance, bottomDistance, leftDistance, rightDistance));

                        Side nearestX = Side.Left;
                        double xDistance = leftDistance;
                        if (Math.Abs(rightDistance) < Math.Abs(leftDistance))
                        {
                            nearestX = Side.Right;
                            xDistance = rightDistance;
                        }

                        Side nearestY = Side.Top;
                        double yDistance = topDistance;
                        if (Math.Abs(bottomDistance) < Math.Abs(topDistance))
                        {
                            nearestY = Side.Bottom;
                            yDistance = bottomDistance;
                        }

                        Side nearest = nearestY;
                        if (xDistance < yDistance)
                            nearest = nearestX;
                        
                        _ball.HitBrick(nearest);
                        brick.Destroy();
                        if (IsGameOver())
                            EndGame();
                    }
                }
            }
        }

        private void CheckForPaddleCollisions(GameTime gameTime, Rectangle ball)
        {
            Rectangle paddle = _paddle.GetCollisionRectangle();
            Rectangle intersection = Rectangle.Intersect(ball, paddle);

            if (intersection.Width != 0 || intersection.Height != 0)
            {
                _ball.HitPaddle(gameTime, _paddle.GetBallModifier(intersection.Location.X));
                if(_demoMode)
                {
                    Random random = new Random();
                    _demoModePaddleOffset = random.Next(14, _paddle.WIDTH - 14);
                }
            }
        }

        private List<Brick> GetBricks(Vector2 position, int number, int space, Color color)
        {
            List<Brick> bricks = new List<Brick>();
            for (int i = 0; i < number; i++)
            {
                if (i > 0)
                    position.X += Brick.WIDTH + space;
                bricks.Add(new Brick(position, color));
            }

            return bricks;
        }

        private bool IsGameOver()
        {
            foreach (Brick brick in _bricks)
                if (brick.Alive)
                    return false;
            return true;
        }

        private void EndGame()
        {
            if (_demoMode)
            {
                if (DemoOver != null)
                    DemoOver(this);
            }
            else
            {
                _gameOver = true;
                _gameOverScreen.FadeInComplete += _gameOverScreen_FadeInComplete;
                _gameOverScreen.FadeIn(1f);                
            }                
        }

        private void _gameOverScreen_FadeInComplete(IGameObject sender)
        {
            var timer = new Timer(10000);
            timer.Elapsed += DemoTimerCompleted;
            timer.AutoReset = false;
            timer.Enabled = true;
        }
    }
}

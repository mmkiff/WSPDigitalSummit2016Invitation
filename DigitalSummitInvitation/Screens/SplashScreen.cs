﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using DigitalSummitInvitation.GameObjects;
using Microsoft.Xna.Framework.Audio;
using System.Timers;
using Microsoft.Xna.Framework.Input;

namespace DigitalSummitInvitation.Screens
{
    public class SplashScreen : IScreen
    {
        public event NextScreenEvent ScreenComplete;
        private Sprite _background;
        private Sprite _wspLogo;
        private Sprite _digitalFactoryLogo;
        private SoundEffect _wspSound;

        public delegate void NextScreenEvent(IScreen sender);

        public SplashScreen()
        {
            _background = new Sprite("whiteBackground", new Vector2(0, 0), 1);
            _wspSound = Game1.ContentManager.Load<SoundEffect>("wsp");
            _wspLogo = new Sprite("wspLogo", new Vector2(11, 156), 0);            
            _wspLogo.FadeInComplete += _wspLogo_FadeInComplete;
            _wspLogo.FadeOutComplete += _wspLogo_FadeOutComplete;

            _digitalFactoryLogo = new Sprite("digitalFactory", new Vector2(0, 0), 0);
            _digitalFactoryLogo.FadeInComplete += _digitalFactoryLogo_FadeInComplete;
            _digitalFactoryLogo.FadeOutComplete += _digitalFactoryLogo_FadeOutComplete;

            _wspLogo.FadeIn(1f);
        }

        private void _digitalFactoryLogo_FadeOutComplete(IGameObject sender)
        {
            if (ScreenComplete != null)
                ScreenComplete(this);
        }

        private void _digitalFactoryLogo_FadeInComplete(IGameObject sender)
        {
            var timer = new Timer(3000);
            timer.Elapsed += DoneDisplayingDigitalFactoryLogo;
            timer.AutoReset = false;
            timer.Enabled = true;
        }

        private void DoneDisplayingDigitalFactoryLogo(object sender, ElapsedEventArgs e)
        {
            _digitalFactoryLogo.FadeOut(0.5f);
            _background.FadeOut(0.5f);
        }

        private void _wspLogo_FadeOutComplete(IGameObject sender)
        {
            _digitalFactoryLogo.FadeIn(1f);
        }

        private void _wspLogo_FadeInComplete(IGameObject sender)
        {
            _wspSound.Play();

            var timer = new Timer(3000);
            timer.Elapsed += DoneDisplayingWSPLogo;
            timer.AutoReset = false;
            timer.Enabled = true;            
        }

        private void DoneDisplayingWSPLogo(object sender, ElapsedEventArgs e)
        {
            _wspLogo.FadeOut(1f);
        }

        public void Draw()
        {
            _background.Draw();
            _wspLogo.Draw();
            _digitalFactoryLogo.Draw();
        }

        public void Update(GameTime gameTime)
        {
            _background.Update(gameTime);
            _wspLogo.Update(gameTime);
            _digitalFactoryLogo.Update(gameTime);
            if (Keyboard.GetState().GetPressedKeys().Count() > 0)
                if (ScreenComplete != null)
                    ScreenComplete(this);
        }
    }
}

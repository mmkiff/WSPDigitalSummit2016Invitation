﻿using DigitalSummitInvitation.Screens;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System;

namespace DigitalSummitInvitation
{
    public class Game1 : Game
    {
        private GraphicsDeviceManager _graphics;
        public static SpriteBatch SpriteBatch;
        public static ContentManager ContentManager;
        public static readonly int WIDTH = 800;
        public static readonly int HEIGHT = 600;
        public static int ACTUAL_WIDTH = 800;

        private IScreen _currentScreen;

        public Game1()
        {
            _graphics = new GraphicsDeviceManager(this);
            _graphics.PreferredBackBufferWidth = WIDTH;
            _graphics.PreferredBackBufferHeight = HEIGHT;
            _graphics.IsFullScreen = true;
            Content.RootDirectory = "Content";
            ContentManager = Content;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            this.Window.Title = "WSP Digital Summit Invitation - Game of the Year Edition";
            this.IsMouseVisible = true;

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            Game1.ACTUAL_WIDTH = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width;
            // Create a new SpriteBatch, which can be used to draw textures.
            SpriteBatch = new SpriteBatch(GraphicsDevice);
            Fonts.BrickFront = Content.Load<SpriteFont>("brickFont-18");

            SplashScreen splashScreen = new SplashScreen();
            splashScreen.ScreenComplete += SplashScreen_ScreenComplete;

            _currentScreen = splashScreen;
        }

        private void StartTitleScreen()
        {
            Console.WriteLine("Starting title screen");
            TitleScreen titleScreen = new TitleScreen();
            titleScreen.StartGame += TitleScreen_StartGame;
            titleScreen.StartDemo += TitleScreen_StartDemo;
            _currentScreen = titleScreen;
        }

        private void StartGameplayScreen(bool demoMode)
        {
            Console.WriteLine("Starting gameplay screen");
            GameplayScreen gameplayScreen = new GameplayScreen(demoMode);
            gameplayScreen.GameOver += GameplayScreen_GameOver;
            gameplayScreen.DemoOver += GameplayScreen_DemoOver;
            _currentScreen = gameplayScreen;
        }

        private void SplashScreen_ScreenComplete(IScreen sender)
        {
            Console.WriteLine("splash screen complete event");
            StartTitleScreen();
        }

        private void TitleScreen_StartDemo(IScreen sender)
        {
            Console.WriteLine("title screen start demo event");
            StartGameplayScreen(true);
        }

        private void GameplayScreen_DemoOver(IScreen sender)
        {
            Console.WriteLine("gameplay screen demo over event");
            StartTitleScreen();
        }

        private void TitleScreen_StartGame(IScreen sender)
        {
            Console.WriteLine("title screen start game event");
            StartGameplayScreen(false);
        }

        private void GameplayScreen_GameOver(IScreen sender)
        {
            Console.WriteLine("gameplay screen game over event");
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                StartTitleScreen();

            _currentScreen.Update(gameTime);

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            SpriteBatch.Begin(SpriteSortMode.Deferred, BlendState.NonPremultiplied );

            _currentScreen.Draw();

            SpriteBatch.End();
            base.Draw(gameTime);
        }
    }
}

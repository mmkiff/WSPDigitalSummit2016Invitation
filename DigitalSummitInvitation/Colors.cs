﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigitalSummitInvitation
{
    public class Colors
    {
        public static Color WSPRed = new Color(229, 44, 55);
        public static Color WSPBlue = new Color(6, 98, 173);
        public static Color DigitalFactoryGrey = new Color(95, 95, 95);

        public static Color InviteTextColor = new Color(62, 62, 62);
    }
}
